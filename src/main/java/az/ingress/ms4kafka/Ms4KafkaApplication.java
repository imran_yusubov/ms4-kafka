package az.ingress.ms4kafka;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@RequiredArgsConstructor
@SpringBootApplication
public class Ms4KafkaApplication implements CommandLineRunner {


    public static void main(String[] args) {
        SpringApplication.run(Ms4KafkaApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

    }
}
